module.exports = {
	'**/*.+(json|yml|yaml|graphql|gql|sass|scss|less|css)': [
		'npx prettier ', 
		'git add'
	],
	'**/*.+(ts|js)': [
		'npx eslint --config .eslintrc.js ', 
		'git add'
	],
}
