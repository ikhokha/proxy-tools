interface MockFileDirectory {
	[key: string]: string
}

const path = jest.genMockFromModule('path') as any


const mockJoin = jest.fn((...paths: string[]): string => {
	let final = ''

	paths.forEach((path)=> {
		if(path.length > 0) {
			final += (path[0]!= '/' ? '/' : '') + path
		}
	})

	return final
})

const mockDirname = jest.fn((path: string): string => {
	let isPath = false
	if(path[path.length-1] == '/') {
		isPath = true	
	}
	

	const pathArr = path.split('/')

	if(!isPath) pathArr.pop() 

	let dirname = ''
	
	pathArr.forEach((pathItem) => {
		if(pathItem.length > 0) {
			dirname += '/' + pathItem
		} 

	})

	return dirname
})

const mockBasename = jest.fn((path: string): string => {
	const pathArr = path.split('/')
	
	return pathArr[pathArr.length -1]
})

const mockExtname = jest.fn((path: string): string => {
	const pathArr = path.split('.')
	
	if(pathArr.length <= 1) return '' 

	return pathArr[pathArr.length -1]
})

path.join = mockJoin
path.dirname = mockDirname
path.basename = mockBasename
module.exports = path