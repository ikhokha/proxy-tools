interface FileList {
	[key: string]: string
}
interface MockFiles {
	[key: string]: {
		[key: string]: string
	}
}

export interface FsMock {
	__setMockFiles: (files: string[])=> void
	existsSync: (path: string)=> boolean
	ReadStream: Function
	WriteStream: Function
	promises: {
		writeFile: (path: string, content: string)=> Promise<void>
	}
}

const fs = jest.genMockFromModule('fs') as any
import * as path from 'path'

// This is a custom function that our tests can use during setup to specify
// what the files on the "mock" filesystem should look like when any of the
// `fs` APIs are used.
let mockFiles: MockFiles = Object.create(null)

function __setMockFiles(newMockFiles: FileList) {
	mockFiles = Object.create(null)

	for(const file in newMockFiles) {

		const dir = path.dirname(file)
		if (!mockFiles[dir]) {
			mockFiles[dir] = {}
		}
		
		mockFiles[dir][path.basename(file)] = newMockFiles[file]
	}
}

function __getMockFiles(): MockFiles {
	return mockFiles
}



function existsSync(filePath: string): boolean {
	const pathString = path.dirname(filePath)
	const fileName = path.basename(filePath)
	if(typeof mockFiles[pathString] !== 'undefined' && typeof mockFiles[pathString][fileName] !== 'undefined') {
		return true
	}

	return false
}

fs.__setMockFiles = __setMockFiles
fs.__getMockFiles = __getMockFiles
fs.existsSync = existsSync

module.exports = fs