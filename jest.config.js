module.exports = {
	...require('./test/config/jest.common'),
	projects: [
		'./test/config/jest.package.js',
		'./test/config/jest.e2e.js'
	]
}