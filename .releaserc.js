module.exports = {
	branch: "master",
	repositoryUrl: process.env.BITBUCKET_PIPELINE_GIT_URL,
	plugins: [
		"@semantic-release/commit-analyzer", 
		"@semantic-release/release-notes-generator", 
		"@semantic-release/npm",
		["@semantic-release/git", {
			"assets": ["package.json"],
			"message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
		}]
	]
}