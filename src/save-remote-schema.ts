import { ApolloLink } from 'apollo-link'
import * as fs from 'fs'
import { printSchema, GraphQLSchema } from 'graphql'
import { introspectSchema } from 'graphql-tools'
import * as validUrl from 'valid-url'
import createProxyLink from './create-proxy-link'
import filterSchema, { OperationsToKeep, TypesToKeep } from './filter-schema'
import path from 'path'


export interface Options {
	url: string
	headers: {
		[key: string]: string
	}
	path: string
	operationsToKeep: OperationsToKeep
	typesToKeep: TypesToKeep
	typeNamePrefix: string
}

export default async (options: Options): Promise<void> => {
	let {url, headers, operationsToKeep, typesToKeep, typeNamePrefix } = options
	let filePath = options.path

	if(typeof url != 'string') throw new Error(`${url} is not a valid string`)
	if(!validUrl.isUri(url)) throw new Error(`${url} is not a valid url`)
	if(typeof filePath != 'string') throw new Error(`${filePath} is not a valid path`)

	const fileName = path.basename(filePath)
	const fileDir = path.dirname(filePath)
	const ext = path.extname(fileName)

	if(!fs.existsSync(fileDir)) throw new Error(`${fileDir} does not exist`)
	if(ext !== 'graphql') throw new Error(`${filePath} must have .graphql as an extension`)


	const link: ApolloLink = createProxyLink(url, headers)
	let schema: GraphQLSchema = await introspectSchema(link)
	schema = filterSchema(schema, operationsToKeep, typesToKeep, typeNamePrefix)
	
	let schemaString: string = printSchema(schema)

	const res = await fs.promises.writeFile(filePath, schemaString)
	console.log('successfully saved schema!')
}

