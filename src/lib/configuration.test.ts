import * as fs from 'fs'
import { findConfigUpwards } from './configuration'

jest.mock('path')
jest.mock('fs')

describe('test findConfigUpwards function', () => {
	const CONFIG_JS_FILENAME = '.awssecretmanager.js'

	beforeEach(() => {
		const files = {
			'/usr/parent/project/.awssecretmanager.js': '{}'
		}

		//eslint-disable-next-line
   		__dirname = "/usr/parent/project/node_modules/@ikhokha/secret-manager/dist";

		//@ts-ignore
		fs.__setMockFiles(files)
		//@ts-ignore
		// console.log(fs.__getMockFiles())
	})

	test('pass a non-existant directory', () => {
		//eslint-disable-next-line
		__dirname = '/fake/directory/does/no/exist'
		expect(() => {
			findConfigUpwards(__dirname, CONFIG_JS_FILENAME)
		}).toThrowErrorMatchingInlineSnapshot(
			'"Could not find .awssecretmanager.js in any parent directory. Please make sure you\'ve added it to your root project directory"'
		)
	})

	test('pass a valid directory', () => {
		expect(findConfigUpwards(__dirname, CONFIG_JS_FILENAME)).toEqual('/usr/parent/project')
	})
})
