import * as fs from 'fs'
import * as path from 'path'

export const findConfigUpwards = (rootDir: string, configFileName: string): string => {
	let dirname = rootDir
	
	//eslint-disable-next-line
	while (true) {
		if (fs.existsSync(path.join(dirname, configFileName))) {
			return dirname
		}

		const nextDir = path.dirname(dirname)
		if (dirname === nextDir) break
		dirname = nextDir
	}

	throw new Error(`Could not find ${configFileName} in any parent directory. Please make sure you've added it to your root project directory`)
}