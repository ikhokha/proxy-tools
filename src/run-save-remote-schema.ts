#! /usr/bin/env node

import saveRemoteSchema from './save-remote-schema'
import {findConfigUpwards} from './lib/configuration'
import path from 'path'

const OPTIONS_FILE_NAME = '.filterschema.js'
const PROJECT_DIR = findConfigUpwards(__dirname, OPTIONS_FILE_NAME)
const optionsFilePath = path.join(PROJECT_DIR, OPTIONS_FILE_NAME)

//eslint-disable-next-line
let options = require(optionsFilePath)

options.path = path.join(optionsFilePath, options.path)

saveRemoteSchema(options).catch((err: Error): void => {
	console.log(err)
})
