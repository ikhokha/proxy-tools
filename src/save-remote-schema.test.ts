import saveRemoteSchema, { Options } from './save-remote-schema'
import path from 'path'
import * as fs from 'fs'

jest.mock('path')
jest.mock('fs')

describe('save remote schema suite', () => {
	const CONFIG_GRAPHQL_FILENAME = 'schema.graphql'
	const PROJECT_DIR = '/usr/parent/project'

	let optionsDefault = {
		url: 'https://www.randomurl.com',
		headers: {
			'Access-Token': '1234'
		},
		path: '/src/schema.graphql',
		operationsToKeep: {
			Query: ['getBooks', 'getBook'],
			Mutation: ['createBook', 'deleteBook'],
			Subscription: []
		},
		typesToKeep: ['ID', 'Float', 'String', 'Int', 'Boolean', 'Book'],
		typeNamePrefix: 'Ikhokha'
	}
	let options: Options = optionsDefault

	beforeEach(() => {
		options = Object.assign({}, optionsDefault)

		const fileKey = path.join(PROJECT_DIR, CONFIG_GRAPHQL_FILENAME)
		let files: any = {}

		files[fileKey] = ''

		//eslint-disable-next-line
    __dirname = path.join(
			PROJECT_DIR,
			'node_modules/random/secrets-manager/dist'
		)

		//@ts-ignore
		fs.__setMockFiles(files)
		//@ts-ignore
		// console.log(fs.__getMockFiles())
	})

	test('invalid URL', async () => {
		options.url = 'fake url'

		await expect(
			saveRemoteSchema(options)
		).rejects.toThrowErrorMatchingInlineSnapshot(
			'"fake url is not a valid url"'
		)
	})

	test('invalid URL type', async () => {
		//@ts-ignore
		options.url = 123

		await expect(
			saveRemoteSchema(options)
		).rejects.toThrowErrorMatchingInlineSnapshot(
			'"123 is not a valid string"'
		)
	})

	test('invalid Path type', async () => {
		//@ts-ignore
		options.path = 123
		await expect(
			saveRemoteSchema(options)
		).rejects.toThrowErrorMatchingInlineSnapshot('"123 is not a valid path"')
	})
})
