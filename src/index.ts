import { ApolloClient, QueryOptions, ApolloQueryResult } from 'apollo-client'
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import { onError } from 'apollo-link-error'
import { ApolloLink, DocumentNode, FetchResult } from 'apollo-link'
import gql from 'graphql-tag'
import fetch from 'node-fetch'

export interface FederationProxyVariables {
	[s: string]: string
}

export interface FederationProxyOptions {
	uri: string
	headers: { [s: string]: string }
}

export default class FederationProxy {
	private client: ApolloClient<NormalizedCacheObject>
	private body: DocumentNode | null
	private variables: FederationProxyVariables
	public constructor(options: FederationProxyOptions) {
		const { uri, headers } = options
		
		const link = ApolloLink.from([
			onError(({ graphQLErrors, networkError }): void => {
				if (graphQLErrors)
					graphQLErrors.map(({ message, locations, path }): void => console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,),)
				if (networkError) console.log(`[Network error]: ${networkError}`)
			}),
			// @ts-ignore
			new HttpLink({
				uri,
				headers,
				fetch
			})
		])
		this.client = new ApolloClient({
			link,
			cache: new InMemoryCache(),
			
		})

		this.variables = {}
		this.body = null
	}

	public setOperation(query: string, variables: FederationProxyVariables) {
		this.variables = variables
		this.body = gql`${query}`
	}

	public async query<T>(): Promise<FetchResult<T>> {
		if(this.body == null) throw new Error(`${this.body} is not a valid value for body object. please make use of setOperation`)
		return this.client.query({
			query: this.body,
			variables: this.variables
		})
	}

	public async mutate<T>(): Promise<FetchResult<T>> {
		if(this.body == null) throw new Error(`${this.body} is not a valid value for body object. please make use of setOperation`)
		return this.client.mutate({
			mutation: this.body,
			variables: this.variables
		})
	}

	
}