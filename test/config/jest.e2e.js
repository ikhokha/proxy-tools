// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path')

module.exports = {
	...require('./jest.common'),
	testEnvironment: 'node',
	displayName: 'e2e',
	testMatch: [
		'<rootDir>/__e2e_tests__/**/*.test.{ts,js}'
	],
	setupFilesAfterEnv: [require.resolve('./../setup-test.e2e.ts')],
}