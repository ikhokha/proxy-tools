// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path')
module.exports = {
	rootDir: path.join(__dirname, '..', '..'),
	transform: {
		'^.+\\.graphql$': 'jest-raw-loader',
		'^.+\\.ts?$': 'ts-jest',

	},
	moduleFileExtensions: ['ts',  'js', 'json', 'node', 'graphql', 'gql'],
	moduleNameMapper: {
		'\\.module\\.css$': 'identity-obj-proxy',
	},
	moduleDirectories: ['node_modules'],
	collectCoverageFrom: [
		'src/**/*.{ts,js}',
		'!src/**/*.test.{ts,js}',
	],
	coveragePathIgnorePatterns: [
		'/node_modules/',
	],
	coverageThreshold: {
		global: {
			statements: 10,
			branches: 10,
			lines: 10,
			functions: 10
		}
	},
	watchPlugins: [
		'jest-watch-typeahead/filename',
		'jest-watch-typeahead/testname',
		'jest-watch-select-projects'
	],
	reporters: [ 'default', 
		[
			'jest-junit',
			{
				output: './test-reports/junit.xml'
			}
		]
	],
	testMatch: [
		'<rootDir>/src/**/*.test.{ts,js}'
	],
	modulePathIgnorePatterns: [
		'<rootDir>/tasks/storage'
	]
}
