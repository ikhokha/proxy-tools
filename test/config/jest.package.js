// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path')

module.exports = {
	...require('./jest.common'),
	testEnvironment: 'node',
	displayName: 'package',
	setupFilesAfterEnv: [require.resolve('./../setup-test.package.ts')],
}