
custom_registry_url=http://localhost:4873
original_npm_registry_url='https://registry.npmjs.org'
#original_npm_registry_url=`npm get registry`
original_yarn_registry_url=`yarn config get registry`
default_verdaccio_package=verdaccio@3.8.2

function startLocalRegistry {
  echo "$"
  # Start local registry
  tmp_registry_log=`mktemp`
  echo "Registry output file: $tmp_registry_log"
  (cd && npx ${VERDACCIO_PACKAGE:-$default_verdaccio_package} -c $1 &>$tmp_registry_log &)
  # Wait for Verdaccio to boot
  grep -q 'http address' <(tail -f $tmp_registry_log)
  

  # Set registry to local registry
  npm set registry "$custom_registry_url"
  yarn config set registry "$custom_registry_url"
  
  # Login so we can publish packages
  (cd && npx npm-auth-to-token@1.0.0 -u user -p password -e user@example.com -r "$custom_registry_url")

  npmrc_file=$(npm config ls -l | grep "userconfig " | head -1 | sed 's/^.* //')
  npmrc_token=$(awk -F '_authToken=' '{print $2}' $npmrc_file)

  export NPM_TOKEN=$npmrc_token
}

function stopLocalRegistry {
  # Restore the original NPM and Yarn registry URLs and stop Verdaccio
  npm set registry "$original_npm_registry_url"
  yarn config set registry "$original_yarn_registry_url"
  rm -rf ./tasks/storage
  rm -rf ./tasks/htpasswd
}
# use the dry run flag so that it doesnt push the version update in our package.json to git
# and wont add a git tag for this version release as we're just testing locally and dont
# want to publish changes from here
function publishToLocalRegistry {
  npm run semantic-release -- --dry-run
}

function installFromLocalRegistry {
	npm install --registry "$custom_registry_url"
}