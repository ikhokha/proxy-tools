#!/bin/bash

# this file is fired from our Dockerfile or docker-compose.yml file.
# it is triggered once our entire docker image is built and is the command we want to execute everytime
# we run docker-compose up. It is directly responsible for create a local npm repo using verdaccio
# publishing our package, going into our e2e test directory and installing the package from our private registry
# in those e2e tests and then executing the test command

set -e

# Start in tasks/ even if run from root directory
cd "$(dirname "$0")"

# App temporary location
# http://unix.stackexchange.com/a/84980
temp_app_path=`mktemp -d 2>/dev/null || mktemp -d -t 'temp_app_path'`

# Load functions for working with local NPM registry (Verdaccio)
source local-registry.sh
# Load functions used to locally install and uninstall our package
source local-install.sh

# a clean up function to execute everytime we quite our script, fail, succeeed or get interrupted
function cleanup {
  echo 'Cleaning up.'
  cd "$root_path"
  # Uncomment when snapshot testing is enabled by default:
  # rm ./packages/react-scripts/template/src/__snapshots__/App.test.js.snap
  rm -rf "$temp_app_path"
  # Restore the original NPM and Yarn registry URLs and stop Verdaccio
  stopLocalRegistry
  #uninstall our package in all of our local tests
  uninstall_locally
}

# Error messages are redirected to stderr
function handle_error {
  echo "$(basename $0): ERROR! An error was encountered executing line $1." 1>&2;
  cleanup
  echo 'Exiting with error.' 1>&2;
  exit 1
}

function handle_exit {
  cleanup
  echo 'Exiting without error.' 1>&2;
  exit
}

# Exit the script with a helpful error message when any error is encountered
trap 'set +x; handle_error $LINENO $BASH_COMMAND' ERR

# Cleanup before exit on any termination signal
trap 'set +x; handle_exit' SIGQUIT SIGTERM SIGINT SIGKILL SIGHUP

# Echo every command being executed
set -x

# Go to root
cd ..
root_path=$PWD

# stop any local registry if it's currently running and remove and dependencies
cleanup

cd "$root_path"

# Start the local NPM registry
startLocalRegistry "$root_path"/tasks/verdaccio.yaml

# Publish the package 
# because semantic-release has a strange --dry-run flag we can't test the following steps locally
# as dry run will skip publishing to git (which is what we want)
# however it also skips publishing the package to our local npm registry (which we dont want)
# we therefore have to tarball the package locally and execute our test scripts
publishToLocalRegistry

# first we run pack locally which will tar our package and return the file name as the package name
package_name=$(pack_locally)

# we then install our package in all of our e2e tests
install_locally ../../"$package_name"

# install our custom package into each of our end to end tests
# see the publish the package commentary as to why the below line is commented out and the above is not
# install_locally "@ikhokha/secrets-manager"

#go into our root directory
cd "$root_path"

#trigger all tests, including our e2e tests
npm run test:e2e:run

#run a final clean up
cleanup
