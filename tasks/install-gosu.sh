#!/bin/bash

#
#
#	TAKEN FROM : https://stackoverflow.com/questions/39077904/is-there-an-easy-way-to-change-to-a-non-root-user-in-bitbucket-pipelines-docker
#
#

GOSU_VERSION=1.10
GNUPGHOME="$(mktemp -d)"

set -x

if hash gosu 2>/dev/null; then
	echo "gosu already installed"
	echo "moving on."
else
	echo "installing gosu"
	apt-get update 
	apt-get install -y --no-install-recommends ca-certificates wget 
	rm -rf /var/lib/apt/lists/* 
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')" 
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch" 
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc" 
	gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu 
	rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc 
	chmod +x /usr/local/bin/gosu 
	gosu nobody true 
	apt-get purge -y --auto-remove ca-certificates wget

	echo "gosu installed"
fi

