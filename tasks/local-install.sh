
# a set of function used to locally package, install and uninstall our npm package

tarball_prefix="ikhokha-proxy-tools-"
npm_package_name="@ikhokha/proxy-tools"

# calls the npm pack command which tarballs our package as if it were ready to be published to npm registry
# it then returns the tarball file name so we can isntall it later by referencing its name
function pack_locally {
	local_package_name="$(npm pack | grep "$tarball_prefix")"
	echo "$local_package_name"
}

# loops through all of our e2e test folders and installs our package
# package name is passed in as an argument
function install_locally {
	#loop through all subfolders in __e2e_tests__
	for d in __e2e_tests__/*; do
		if [ -d "$d" ]; then
			echo "$d"
			# change into each directory and install all dependencies needed
			cd "$d"
			npm install
			# finally install our package as the final dependency
			npm install -s "$1" 

			cd ../../
		fi
	done
}

# loops through all of our e2e test folders and uninstalls our package
# package name is passed in as an argument
function uninstall_locally {
	#first, check if a tarball file exists in the current folder
	if $(find ./ -type f -print | grep -q $tarball_prefix); then
		echo "file does exist, continue clean up"
		# if the file does exist, get its full name
		file="$(find ./ -type f -print | grep $tarball_prefix)"
		file=${file//.\/\//}
		#delete the file so it doesnt get added to any git commits, etc.
		rm -rf "$file"
	else
		echo "file does not exist, nothing to clean"
	fi  

	#loop through all subfolders in __e2e_tests__
	for d in __e2e_tests__/*; do
		if [ -d "$d" ]; then
			echo "$d"
			# change to that directory and delete our package
			cd "$d"
			npm uninstall "$npm_package_name" --save
			rm -rf ./node_modules

			cd ../../
		fi
	done
}

