FROM node:10.16.0

WORKDIR /home/node/app

RUN chown -R node:node /home/node/app

USER node

ARG BITBUCKET_PIPELINE_GIT_URL
ENV BITBUCKET_PIPELINE_GIT_URL="$BITBUCKET_PIPELINE_GIT_URL"
ARG GIT_CREDENTIALS
ENV GIT_CREDENTIALS="$GIT_CREDENTIALS"

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY --chown=node:node package.json .

COPY --chown=node:node package-lock.json .

RUN npm install --ignore-scripts

# Bundle app source
COPY --chown=node:node . .

RUN npm run build

CMD []


