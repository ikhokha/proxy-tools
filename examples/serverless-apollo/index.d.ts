
declare module 'serverless-offline'
declare module 'serverless-webpack'
declare module 'custom-env' 
declare module 'aws-lambda-test-utils'
declare module 'aws-event-mocks'
declare module '*.json' {
	const content: any
	export default content
}

declare module '*.graphql' {
	const content: any
	export default content
}

declare namespace NodeJS {
	interface ProcessEnv {
		
	}
}
