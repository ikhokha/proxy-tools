require('dotenv').config()

module.exports = {
	url: process.env.API_URL,
	operationsToKeep: {
		Query: [
			// queries to keep
			'continents',
			'country'
			
		],
		Mutation: [
			// mutations to keep
		]
	},
	typesToKeep: [
		'Continent',
		'String',
		'Int',
		'Language',
		'Query',
		'Country'
	],
	typeNamePrefix: 'ContinentService',
	outputFile: './src/schema.graphql',
	headers: {
		//this isn't needed for this example but illustrative
		'Authorisation': process.env.API_TOKEN
	}
}