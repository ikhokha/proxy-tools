import { ApolloServer, gql } from 'apollo-server-lambda'
import { APIGatewayProxyEvent, Callback, APIGatewayProxyResult } from 'aws-lambda'
import { Context as LambdaContext } from 'aws-lambda'
import typeDefs from './schema.graphql'
import { buildFederatedSchema } from '@apollo/federation'
import resolvers from './resolvers'
import { GraphQLResolverMap } from '@apollographql/apollo-tools'
import FederationProxy from '@ikhokha/proxy-tools/dist'

export interface CustomContext {
	federationProxy: FederationProxy
}



function createServer(): (event: APIGatewayProxyEvent, context: LambdaContext, callback: Callback<APIGatewayProxyResult>)=> void {
	//there seems to be some typescript inconsistencies in the apollo-server-lambda and buildFederatedSchema... but it still works
	//@ts-ignore
	const schema = buildFederatedSchema([{
		typeDefs: gql`${typeDefs}`,
		resolvers: resolvers as GraphQLResolverMap<CustomContext>
	}])


	const server = new ApolloServer({ 
		schema,
		introspection: true,
		playground: true,
		context: async (req: {event: APIGatewayProxyEvent}): Promise<CustomContext> => {
			const body = JSON.parse(req.event.body as string)
			const federationProxy = new FederationProxy({
				uri: process.env.API_URL as string,
				headers: {
					//please don't ever do this in production!!! For secrets, use a real secret store.
					'Authorisation': process.env.API_KEY as string
				}
			})

			federationProxy.setOperation(body.query, body.variables)

			return {
				federationProxy
			}
		}
	})
	return server.createHandler({
		cors: {
			origin: true,
			credentials: true
		}
	})
}


export default createServer