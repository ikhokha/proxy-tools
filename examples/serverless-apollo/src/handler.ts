import createServer from './create-server'
import { APIGatewayProxyEvent, Callback, APIGatewayProxyResult, Context } from 'aws-lambda'

export const appFunction: (event: APIGatewayProxyEvent, context: Context, callback: Callback<APIGatewayProxyResult>)=> void = createServer()
