import { Resolvers, ContinentServiceContinent, Maybe, ResolversTypes, ContinentServiceCountry } from './generated'
import { CustomContext } from './create-server'

const resolvers: Resolvers<CustomContext> ={ 
	Query: {
		continents: async (_parent, _args, ctx, _info): Promise<ContinentServiceContinent[] | null> => {
			const results = await ctx.federationProxy.query<{continents: ContinentServiceContinent[]}>()

			if(typeof results.data == 'undefined') throw new Error('Could not find any continents}')

			return results.data.continents
		},
		country: async (_parent, _args, ctx, _info): Promise<ContinentServiceCountry | null> => {
			const results = await ctx.federationProxy.query<{country: ContinentServiceCountry | null}>()

			if(typeof results.data == 'undefined') throw new Error('Could not find any country with that handle}')

			return results.data.country
		},
	}
}

export default resolvers

