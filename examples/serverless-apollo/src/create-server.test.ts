import createServer from './create-server'
import * as apolloServerLambda from 'apollo-server-lambda'

// @ts-ignore
apolloServerLambda.ApolloServer = jest.fn(() => {
	return {
		createHandler: jest.fn()
	}
})

describe('test create server', () => {

	beforeEach(() => {

	})

	test('successfully start server', () => {
		expect(createServer()).toBe(undefined)
	})
})