import { GraphQLResolveInfo } from "graphql";
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type ContinentServiceContinent = {
  __typename?: "ContinentServiceContinent";
  code?: Maybe<Scalars["String"]>;
  name?: Maybe<Scalars["String"]>;
  countries?: Maybe<Array<Maybe<ContinentServiceCountry>>>;
};

export type ContinentServiceCountry = {
  __typename?: "ContinentServiceCountry";
  code?: Maybe<Scalars["String"]>;
  name?: Maybe<Scalars["String"]>;
  native?: Maybe<Scalars["String"]>;
  phone?: Maybe<Scalars["String"]>;
  continent?: Maybe<ContinentServiceContinent>;
  currency?: Maybe<Scalars["String"]>;
  languages?: Maybe<Array<Maybe<ContinentServiceLanguage>>>;
  emoji?: Maybe<Scalars["String"]>;
  emojiU?: Maybe<Scalars["String"]>;
};

export type ContinentServiceLanguage = {
  __typename?: "ContinentServiceLanguage";
  code?: Maybe<Scalars["String"]>;
  name?: Maybe<Scalars["String"]>;
  native?: Maybe<Scalars["String"]>;
  rtl?: Maybe<Scalars["Int"]>;
};

export type Query = {
  __typename?: "Query";
  continents?: Maybe<Array<Maybe<ContinentServiceContinent>>>;
  country?: Maybe<ContinentServiceCountry>;
};

export type QueryCountryArgs = {
  code?: Maybe<Scalars["String"]>;
};

export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, TParent, TContext, TArgs>;
}

export type SubscriptionResolver<
  TResult,
  TParent = {},
  TContext = {},
  TArgs = {}
> =
  | ((
      ...args: any[]
    ) => SubscriptionResolverObject<TResult, TParent, TContext, TArgs>)
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<
  TResult = {},
  TParent = {},
  TContext = {},
  TArgs = {}
> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Query: ResolverTypeWrapper<{}>;
  ContinentServiceContinent: ResolverTypeWrapper<ContinentServiceContinent>;
  String: ResolverTypeWrapper<Scalars["String"]>;
  ContinentServiceCountry: ResolverTypeWrapper<ContinentServiceCountry>;
  ContinentServiceLanguage: ResolverTypeWrapper<ContinentServiceLanguage>;
  Int: ResolverTypeWrapper<Scalars["Int"]>;
  Boolean: ResolverTypeWrapper<Scalars["Boolean"]>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Query: {};
  ContinentServiceContinent: ContinentServiceContinent;
  String: Scalars["String"];
  ContinentServiceCountry: ContinentServiceCountry;
  ContinentServiceLanguage: ContinentServiceLanguage;
  Int: Scalars["Int"];
  Boolean: Scalars["Boolean"];
};

export type ContinentServiceContinentResolvers<
  ContextType = any,
  ParentType = ResolversParentTypes["ContinentServiceContinent"]
> = {
  code?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  countries?: Resolver<
    Maybe<Array<Maybe<ResolversTypes["ContinentServiceCountry"]>>>,
    ParentType,
    ContextType
  >;
};

export type ContinentServiceCountryResolvers<
  ContextType = any,
  ParentType = ResolversParentTypes["ContinentServiceCountry"]
> = {
  code?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  native?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  phone?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  continent?: Resolver<
    Maybe<ResolversTypes["ContinentServiceContinent"]>,
    ParentType,
    ContextType
  >;
  currency?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  languages?: Resolver<
    Maybe<Array<Maybe<ResolversTypes["ContinentServiceLanguage"]>>>,
    ParentType,
    ContextType
  >;
  emoji?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  emojiU?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
};

export type ContinentServiceLanguageResolvers<
  ContextType = any,
  ParentType = ResolversParentTypes["ContinentServiceLanguage"]
> = {
  code?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  native?: Resolver<Maybe<ResolversTypes["String"]>, ParentType, ContextType>;
  rtl?: Resolver<Maybe<ResolversTypes["Int"]>, ParentType, ContextType>;
};

export type QueryResolvers<
  ContextType = any,
  ParentType = ResolversParentTypes["Query"]
> = {
  continents?: Resolver<
    Maybe<Array<Maybe<ResolversTypes["ContinentServiceContinent"]>>>,
    ParentType,
    ContextType
  >;
  country?: Resolver<
    Maybe<ResolversTypes["ContinentServiceCountry"]>,
    ParentType,
    ContextType,
    QueryCountryArgs
  >;
};

export type Resolvers<ContextType = any> = {
  ContinentServiceContinent?: ContinentServiceContinentResolvers<ContextType>;
  ContinentServiceCountry?: ContinentServiceCountryResolvers<ContextType>;
  ContinentServiceLanguage?: ContinentServiceLanguageResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
};

/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
 */
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
